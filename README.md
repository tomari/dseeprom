# Nintendo DS cartridge SPI EEPROM eraser

This program uses Altera USB Blaster programming cable to erase save-data SPI EEPROM contained in Nintendo DS Gamepaks.
The save-data SPI EEPROM is separate from the main game program ROM, so it is generally safe to modify contents of the save-data ROM.
I haven't came across one for myself, but there are Gamepaks with Flash save-rom instead of SPI EEPROM.
For those cartridges, the Flashrom program can be used with the same cable/socket.


## Requirements

- CentOS 7 or similar system
- Compiler with C++11 support
- libftdi-0.20
  - https://www.intra2net.com/en/developer/libftdi/
  - libftdi-1.x series do not work.
- Altera USB Blaster FPGA/CPLD programming cable

## What it does

Fills first 16 bytes of the save-rom with 0xFF.
This breaks signature there.
On next boot, the game identifies broken signature, and initializes the whole rom contents to factory state.

## Connection

```
 USB Blaster ---  DS Gamepak  
  1=DCLK --------- SCLK =2  
  4=VCC(TRGT) ---- VCC  =8  -- Connect to 3.3 V power supply
  7=DATAOUT ------ MISO =15  
  8=nCS ---------- /CS  =6  
  9=ADSI --------- MOSI =16  
  2=GND ---------- GND  =1  
 10=GND ---------- GND  =17
```

```
 +-------------------\
 | Nintendo DS      =| 17
 | Cartridge        =|
 | Connector side   =|
 |                  =|
 |                  =| 1
 +-------------------+
```

## Limitations

This program does not read the contents of the save-rom.
Because I don't need that.

## See also

1. Altera. USB-Blaster Download Cable User Guide. UG-USB81204. 2015.08.20.
2. NintendoDSのカートリッジのセーブデータの解析 http://www.kako.com/neta/2004-018/2004-018.html
3. usbblaster_spi.c https://github.com/stefanct/flashrom/blob/master/usbblaster_spi.c


