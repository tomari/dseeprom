/* Nintendo DS EEPROM Eraser.
 * Copyright (C) 2016 Hisanobu Tomari. 
 * 
 * The controlling method here is a modification of usbblaster_spi.c
 * from the flashrom project. The modification is for handling SPI EEPROM
 * instead of SPI Flash memory.
 * 
 * The copyright notice of the usbblaster_spi.c follows:
 * 
 * Copyright (C) 2012 James Laird <jhl@mafipulation.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */
#include <iostream>
#include <stdexcept>
#include <vector>
#include <thread>
#include <chrono>
#include <cstring>
#include <ftdi.h>

class DSeeprom {
public:
	DSeeprom();
	~DSeeprom();
	void spi_cmd(std::vector<uint8_t>& write_data, std::vector<uint8_t>& read_data);
	void read_data(unsigned long addr);
	void fill_16_bytes();
private:
	struct ftdi_context ftdic;
	const int ALTERA_VID=0x09fb;
	const int ALTERA_USBBLASTER_PID=0x6001;
	const long BUFSIZE=64L;
	const uint8_t BIT_BYTE=(1U<<7);  // byte mode (rather than bitbang)
	const uint8_t BIT_READ=(1U<<6);  // read request
	const uint8_t BIT_LED =(1U<<5);
	const uint8_t BIT_CS  =(1U<<3);
	const uint8_t BIT_TMS =(1U<<1);
	const uint8_t BIT_CLK =(1U<<0);

	uint8_t reverse(uint8_t b);
	void send_write(std::vector<uint8_t>& write_data);
	void send_read(std::vector<uint8_t>& read_buf);
};

DSeeprom::DSeeprom() {
	if(ftdi_init(&ftdic)<0) {
		throw std::runtime_error("ftdi_init failed");
	}
	if(ftdi_usb_open(&ftdic,ALTERA_VID, ALTERA_USBBLASTER_PID) <0) {
		throw std::runtime_error("ftdi_usb_open failed");
	}
	if(ftdi_usb_reset(&ftdic)<0) {
		throw std::runtime_error("ftdi_usb_reset failed");
	}
	if(ftdi_set_latency_timer(&ftdic,2)<0) {
		throw std::runtime_error("ftdi_set_latency_timer failed");
	}

	uint8_t buf[BUFSIZE+1];
	std::memset(buf, 0, sizeof buf);
	buf[sizeof(buf)-1]=BIT_LED|BIT_CS;
	if(ftdi_write_data(&ftdic,buf,sizeof buf)<0) {
		throw std::runtime_error("USB-Blaster reset write failed");
	}
	if(ftdi_read_data(&ftdic,buf,sizeof buf)<0) {
		throw std::runtime_error("USB-Blaster reset read failed");
	}
}

DSeeprom::~DSeeprom() {
	ftdi_usb_close(&ftdic);
}

void
DSeeprom::spi_cmd(std::vector<uint8_t>& write_data, std::vector<uint8_t>& read_data) {
	auto cmd=BIT_LED;
	if(ftdi_write_data(&ftdic, &cmd, 1)<0) {
		throw std::runtime_error("USB-Blaster enabls CS failed");
	}
	if(write_data.size()>0) {
		send_write(write_data);
	}
	if(read_data.size()>0) {
		send_read(read_data);
	}

	cmd=BIT_CS;
	if(ftdi_write_data(&ftdic, &cmd, 1) < 0) {
		throw std::runtime_error("USB-Blaster disable CS failed");
	}
}

/* The programmer shifts bits in the wrong order for SPI, so we use this method 
to reverse the bits when needed.
* http://graphics.stanford.edu/~seander/bithacks.html#ReverseByteWith32Bits */
uint8_t
DSeeprom::reverse(uint8_t b) {
	return ((b * 0x0802LU & 0x22110LU) | (b * 0x8020LU & 0x88440LU)) * 0x10101LU >> 16;
}

void
DSeeprom::send_write(std::vector<uint8_t>& write_data) {
	uint8_t buf[BUFSIZE];
	uint8_t len=static_cast<uint8_t>(write_data.size());
	buf[0]=BIT_BYTE | len;
	for(auto i=0ul; i<len; i++) {
		buf[i+1]=reverse(write_data[i]);
	}
	if(ftdi_write_data(&ftdic,buf,len+1)<0) {
		throw std::runtime_error("USB-Blaster write failed");
	}
}

void
DSeeprom::send_read(std::vector<uint8_t>& read_buf) {
	uint8_t buf[BUFSIZE];
	auto len=static_cast<uint8_t>(read_buf.size());
	std::memset(buf,0,sizeof buf);
	buf[0]=BIT_BYTE|BIT_READ|len;
	if(ftdi_write_data(&ftdic,buf,1)<0) {
		throw std::runtime_error("USB-Blaster write failed");
	}
	std::this_thread::sleep_for(std::chrono::milliseconds(10));
	if(ftdi_read_data(&ftdic, buf, len) <0) {
		throw std::runtime_error("USB-Blaster read failed");
	}
	for(auto i=0ul; i<len; i++) {
		read_buf[i]=reverse(buf[i]);
	}
}

void
DSeeprom::read_data(unsigned long addr) {
	std::vector<uint8_t> read_cmd(3);
	std::vector<uint8_t> res(16);
	read_cmd[0]=0x3;
	read_cmd[1]=0x00;
	read_cmd[2]=0x00;
	spi_cmd(read_cmd, res);
	for(auto i=0ul; i<res.size(); i++) {
		std::cout <<res[i];
	}
}

void
DSeeprom::fill_16_bytes() {
	std::vector<uint8_t> unlock_cmd(1);
	unlock_cmd[0]=0x06;
	std::vector<uint8_t> write16_cmd(19);
	write16_cmd[0]=0x02;
	write16_cmd[1]=0x00;
	write16_cmd[2]=0x00;
	for(auto i=3ul; i<write16_cmd.size(); i++) {
		write16_cmd[i]=0xff;
	}
	std::vector<uint8_t> read_bogo(0);
	spi_cmd(unlock_cmd, read_bogo);
	spi_cmd(write16_cmd, read_bogo);
}


int main(int argc, char *argv[]) {
	auto rom=DSeeprom();
	std::cerr << "initialized" << std::endl;
	rom.fill_16_bytes();
	return 0;
}
